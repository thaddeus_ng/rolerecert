package roleRecert;

import java.io.*;
import java.util.*;

import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.*;

import javax.swing.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;

class excelbook
{
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	HSSFCellStyle style;
	
	String name;
	Object[] headers;
	public void createExcelbook(String workbookName){
		System.out.println("Create New ExcelBook named "+workbookName);
		workbook = new HSSFWorkbook(); 
		name = workbookName+".xls";
	}
	
	public void setHeader(Object[] header){
		headers=header;
	}
	
	public void setCurrentSheet(String sheetName){
		if(sheetName.length()>20)
			sheetName = sheetName.substring(0, 20);
		
		for(int x=0;x<workbook.getNumberOfSheets();x++){
			System.out.println("---Excel Sheet "+x+"---"+workbook.getSheetName(x));
		}
		
		if(workbook.getSheet(sheetName)==null){
			sheet = workbook.createSheet(sheetName);
			addStyledHeader();
			System.out.println("---New Sheet"+sheet.getSheetName()+"---");
		}else{
			sheet=workbook.getSheet(sheetName);
			System.out.println("---Existing Sheet"+sheet.getSheetName()+"---");
			}
	}
	
	public void addStyledHeader(){
		applyHeaderStyle();
		Row row=sheet.createRow(0);
		int cellnum=0;
		for (Object obj : headers)
		{//write to cell
			Cell cell = row.createCell(cellnum++);
			cell.setCellValue((String)obj);
			cell.setCellStyle(style);
			}
	}
	public void addFront(Map<String, Object[]> data){
		//Iterate over data and write to sheet
		Set<String> keyset = data.keySet();
		int rownum =1;
		System.out.println("Starting at"+rownum);
		for (String key : keyset)
		{//write to row
			Row row = sheet.createRow(rownum++);
			Object [] objArr = data.get(key);
			int cellnum = 0;
			String out="";
			for (Object obj : objArr)
			{//write to cell
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Integer)
					cell.setCellValue((Integer)obj);
				out+="| "+(String)obj;
				}
			System.out.println("---ROW READ---"+out);
			}
		}
	
	public void resizeColumns(){
		for(int x=0;x<workbook.getNumberOfSheets();x++){
			sheet = workbook.getSheetAt(x);
			int count=0;
			for(Cell cell : sheet.getRow(0)){
				sheet.autoSizeColumn(count);
				count++;
			}
		}
	}
	
	public void applyHeaderStyle(){
        style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setColor(HSSFColor.BLACK.index);
        font.setFontHeightInPoints((short)12);
        font.setBold(true);
        style.setFont(font);
	}
	
	public HSSFWorkbook getWorkbook(){
		return workbook;
	}
	
	public String getFileName(){
		return name;
	}
	
	public HSSFSheet getActiveSheet(){
		return sheet;
	}
}

class emlMessage{
	Message message;
	MimeBodyPart content;
	Multipart multipart;
	
	String fileName;
	public emlMessage(){
		message = new MimeMessage(Session.getInstance(System.getProperties()));
		content = new MimeBodyPart();
		multipart = new MimeMultipart();
	}
	
	public void setFileName(String fileName){
		this.fileName=fileName+".eml";
	}
	
	public void setEmailTo(String to) throws AddressException, MessagingException{
		System.out.println(to);
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	}
	
	public void setEmailFrom(String from)throws AddressException, MessagingException{
		message.setFrom(new InternetAddress(from));
	}
	
	public void setBody(String body) throws MessagingException{
		content.setText(body);
		multipart.addBodyPart(content);
	}
	
	public void setAttachmentList(List<File> attachments) throws MessagingException{
		for(File file : attachments) {
            MimeBodyPart attachment = new MimeBodyPart();
			FileDataSource source = new FileDataSource(file);
            attachment.setDataHandler(new DataHandler(source));
            attachment.setFileName(file.getName());
            multipart.addBodyPart(attachment);
        }
	}
	
	public void setAttachment(File file) throws MessagingException{
		MimeBodyPart attachment = new MimeBodyPart();
		FileDataSource source = new FileDataSource(file);
        attachment.setDataHandler(new DataHandler(source));
        attachment.setFileName(file.getName());
        multipart.addBodyPart(attachment);
	}
	
	public void toFile() throws MessagingException, FileNotFoundException, IOException{
		message.setContent(multipart);
        message.writeTo(new FileOutputStream(new File(fileName)));
	}
}

public class roleRecert {
	
	public roleRecert(int groupByCellId, String output, int emailCell, int employeeNameCell, Object[] headers, String emlFrom, String emlSubj, String emlBody) throws IOException, AddressException, MessagingException{
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("."));

		int r = fc.showOpenDialog(new JFrame());
        if (r == JFileChooser.APPROVE_OPTION) {
          File currFile = fc.getSelectedFile();
          
          FReader filereader = new FReader(currFile);
          //dummy value for manager
          String manager = "Dummy";
          String managerEmail = "Dummy";
          String employee = "Dummy";
          int count=0;
          int insert=1;
          excelbook workbook = null;
          boolean firstRow=true;
          Map<String, Object[]> data = new TreeMap<String, Object[]>();
          // loop through file
          
          for(String temp: filereader.getContent()){          
        	  //split cell into array and get the current manager 
        	  Object [] currRow = temp.split("\t");
        	  
        	  if(currRow.length<groupByCellId || currRow.length<emailCell)
        	  {
        		  System.out.println("group by cell id or email cell is outside of expected parameters");
        		  break;
        	  }else{
        	  //if manager is new, create new workbook, sheet, set current manager to new manager and reset excel insert to 1;
        	  System.out.println("---compare---Read Manager"+manager+":    Compared to Manager"+currRow[groupByCellId]);
        	  if(manager.equalsIgnoreCase((String) currRow[groupByCellId])==false){
            	  manager = (String) currRow[groupByCellId];
            	  managerEmail = (String) currRow[emailCell];
            	  employee = (String) currRow[employeeNameCell];
            	  
            	  System.out.println("---new manager ---new Manager:  "+manager+",    new employee:  "+employee);
  	        	  workbook = new excelbook();
  	        	  workbook.createExcelbook(manager);
  	        	  workbook.setHeader(headers);
  	        	  workbook.setCurrentSheet(employee);
            	  
            	  insert=1;
            	  
            	  //This data needs to be written (Object[])
            	  data = new TreeMap<String, Object[]>();
        	  }
        	  
        	  //System.out.println("Insert into dataset"+insert);
        	  if(!firstRow){
        		  for(int x=0;x<currRow.length;x++){
        			  Object[] trimmed = new Object[currRow.length];
        			  trimmed[3]=currRow[3];
        			  trimmed[4]=currRow[4];
        			  data.put(insert+"", trimmed);
        		  }
        	  }else{
        		  data.put(insert+"", currRow);
        		  firstRow=false;
        	  }
        	  System.out.println((String) currRow[0]+ "&"+(String) currRow[4]);
  	          insert++;
  	          
  	          //check next value, if it doesn't match flip flag to close this workbook
  	          String[] tempArray = null;
  	          boolean managerMatch =true;
  	          boolean employeeMatch=true;
  	          System.out.println("current"+count+": max "+filereader.getContent().size());
  	          
  	          if(count+1<filereader.getContent().size())
  	          {
  	        	  tempArray = filereader.getContent().get(count+1).split("\t");
  	        	  managerMatch = manager.equalsIgnoreCase(tempArray[groupByCellId]);
  	        	  //System.out.println("Manager:   "+manager+" next manager:   "+ tempArray[groupByCellId]+"result:  "+managerMatch);
  	        	  employeeMatch = employee.equalsIgnoreCase(tempArray[employeeNameCell]);
  	        	  //System.out.println("Employee:   "+employee+" next employee:   "+ tempArray[employeeNameCell]+"result:  "+employeeMatch);
  	        	  
  	        	  if(!employeeMatch){
  	  	        	workbook.addFront(data);
  	  	        	data.clear();
  	  	        	firstRow=true;
  	  	        	if(managerMatch){
  	  	        		workbook.setCurrentSheet(tempArray[employeeNameCell]);
  	  	        		employee = tempArray[employeeNameCell];
  	  	        	}
  	  	          }
  	          }else{
  	        	  workbook.addFront(data);
  	        	  data.clear();
  	        	  }
  	            	          
  	          //if this is the last row or if the flag checking for manager match is false close print/close workbook
  	          if(count+1>=filereader.getContent().size()||managerMatch==false){
  	        	  workbook.resizeColumns();
  	  	        FileOutputStream out;
  	  	        	switch(output){
  	  	        	//switch on submitted output, eml creates email files, xls creates excel files
  	  	        	case "eml":
  	  	        		File tempFile = new File("_"+workbook.getFileName());
  	  	  	            out = new FileOutputStream(tempFile);
  	  	  	            workbook.getWorkbook().write(out);
  	  	  	            out.close();
  	  	  	            workbook.getWorkbook().close();
  	  	        		//call create message to generate eml file
  	  	        		emlMessage currEml = new emlMessage();
  	  	        		currEml.setFileName("_"+manager);
  	  	        		currEml.setEmailTo(managerEmail);
  	  	        		currEml.setEmailFrom(emlFrom);
  	  	        		currEml.setBody("Dear" + manager+emlBody);
  	  	        		currEml.setAttachment(tempFile);
  	  	        		currEml.toFile();
  	  	        		//delete temp excel files
  	  	        		if(tempFile.delete())
  	  	        			System.out.println(tempFile.getName() + " is deleted!");
  	  	        		else
  	  	        			System.out.println("Delete operation is failed.");
  	  	        		break;
  	  	        	case "xls":
  	  	  	            //Write the workbook in file system
  	  	  	            out = new FileOutputStream(new File("_"+manager+"."+output));
  	  	  	            workbook.getWorkbook().write(out);
  	  	  	            out.close();
  	  	  	            workbook.getWorkbook().close();
  	  	  	            System.out.println(manager+".xls written successfully on disk.");
  	  	  	            break;
  	  	        	}
  	        }
  	        count++;
          //*****end paste
          }//if statement catching for groupby or email cells outside of range
          }
        }
	}
	
	/*createMessage(
	String filepath <must include extension, 
	String to <leave as do not reply, makes it easier to just reply to manually forward email>, 
	string from <sender>, 
	String subject, 
	String body,
	List of files containing desired attachments
	*/
	
	//initialize map with String headers
	public Map<String, Object[]> createMap(){
		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		return data;
	}

	public static void main(String[] args) throws IOException, AddressException, MessagingException {
		// TODO Auto-generated method stub
		Object [] headers = {"employee name", "employee id","employee title","role id","role description","supervisor name","supervisor id","office", "supervisor email", "comments"};
		String sender="do_not_reply@savechildren.org";
		String subject = "Agresso Role Recertification"; 
		String body="\nPlease find the Excel file attached with all direct reports and associated roles."
			+ "\nPlease Verify the roles are correct. If there is any disparity please contact <<Insert current role recert manager here>> ASAP so we can make the appropriate adjustments."
			+ "\nThank you\nAgresso Administration"; 
		roleRecert currFile= new roleRecert(5,"eml", 8, 0, headers, sender, subject, body);
		System.out.println("Test after roleRecert");
		System.exit(0);
	}
}

