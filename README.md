# rolerecert
Developed to automate the annual Role Recertification Process. 

Built in Java using Eclipse IDE, Maven for package management, Apache POI for Excel file manipulation, JavaMail for custom .eml file generation, and a small file reader for parsing text documents. 

Functions:
Reading/Splitting a deliminated text file into separate excel files based on column values
Packaging files into custom .eml format email files for distribution.
